//
//  ViewController.m
//  花名册
//
//  Created by yutao on 15/7/19.
//  Copyright (c) 2015年 yutao. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    self.view.backgroundColor = [UIColor whiteColor];
    self.array = [NSMutableArray arrayWithObjects:
                  @"张勇",
                  @"王帅",
                  @"安庆华",
                  @"韩晓光",
                  @"金洪健",
                  @"徐申",
                  @"李克勤",
                  @"刘学",
                  @"孙亚楠",
                  @"刘洋",
                  @"王涛",
                  @"邸雅楠",
                  @"唐顺成",
                  @"付泽成",
                  @"樊志康",
                  @"刘汉",
                  @"王博",
                  @"李源",
                  @"潘强",
                  @"赵卉",
                  @"吴神",
                  @"大水杯22",
                  @"豆沙包22",
                  @"登山包22",
                  @"第三步",
                  @"大手2222",
                  

                  nil];

    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 667, 375)];
    self.label.backgroundColor = [UIColor whiteColor];
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.text = @"Duang";
    self.label.font = [UIFont systemFontOfSize:60];
    self.label.userInteractionEnabled = YES;
    [self.view addSubview:self.label];

    self.selected = NO;
    NSLog(@"count = %ld",self.array.count);



}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{


    if (self.selected == NO) {

        self.time = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(changeName) userInfo:nil repeats:YES];

    }else
    {
        [self.time invalidate];
        [self.array removeObject:self.studentName];
    }

    self.selected = !self.selected;

}

-(void)changeName
{
    NSLog(@"count = %ld",self.array.count);
    NSInteger index = arc4random() % self.array.count;
    self.studentName = [self.array objectAtIndex:index];
    self.label.text = self.studentName;
    
}














//








@end
