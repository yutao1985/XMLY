//
//  ViewController.h
//  花名册
//
//  Created by yutao on 15/7/19.
//  Copyright (c) 2015年 yutao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property(nonatomic,retain)NSMutableArray *array;
@property(nonatomic,retain)UILabel *label;
@property(nonatomic,retain)NSTimer *time;
@property(nonatomic,assign)BOOL selected;

@property(nonatomic,copy)NSString *studentName;
@end

