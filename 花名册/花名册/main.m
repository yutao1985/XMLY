//
//  main.m
//  花名册
//
//  Created by yutao on 15/7/19.
//  Copyright (c) 2015年 yutao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
